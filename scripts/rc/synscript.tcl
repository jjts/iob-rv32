set_attribute lib_search_path [list /opt/ic_tools/pdk/faraday/umc130/HS/fsc0h_d/2009Q1v3.0/GENERIC_CORE/FrontEnd/synopsys]
set_attribute hdl_search_path {list ../../ ../../picosoc/ ../quartus}
set_attribute library [list fsc0h_d_generic_core_tt1p2v25c.lib]

read_hdl -v2001 \
picorv32.v \
simpleuart.v \
system.v

elaborate system
define_clock -name clk -period 5000 [find / -port clk] 
synthesize -to_mapped
#retime -prepare -min_delay
report gates > gates_report.txt
report timing > timing_report.txt
write_hdl -mapped > system_synth.v 
write_sdc > system_synth.sdc
exit

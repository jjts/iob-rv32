set_attribute lib_search_path {/opt/ic_tools/pdk/faraday/umc130/HS/fsc0h_d/2009Q1v3.0/GENERIC_CORE/FrontEnd/synopsys ../asic_memories}
set_attribute hdl_search_path {list ../../ ../../picosoc/ ../quartus}
set_attribute library [list fsc0h_d_generic_core_tt1p2v25c.lib]

read_netlist system_synth.v
read_tcf ../ncsim/system.tcf 

report power > power_report.txt
exit

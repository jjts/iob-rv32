`timescale 1 ns / 1 ps

module system (
               //cpu
	       input      clk,
	       input      resetn,
	       output reg led,
	       output     ser_tx,
               output     trap,

               //spi
               input      sclk
               );
   // 4096 32bit words = 16kB memory
   parameter MEM_ADDR_W = 12;

   wire                         mem_valid;
   wire                         mem_instr;
   reg                          mem_ready;
   wire                         mem_ready_nxt;
   wire [31:0]                  mem_addr;
   wire [31:0]                  mem_wdata;
   wire [3:0]                   mem_wstrb;
   reg [31:0]                   mem_rdata;

   wire                         mem_la_read;
   wire                         mem_la_write;
   wire [31:0]                  mem_la_addr;
   wire [31:0]                  mem_la_wdata;
   wire [3:0]                   mem_la_wstrb;

   //peripherals

   //memory
   reg                          mem_sel;
   wire [31:0]                  mem_read_data;

   //uart 
   reg                          uart_sel;
   wire [31:0]                  uart_read_data;
   //uart 
   wire                         ser_rx = 1'b0;
   
   //led
   reg                          led_sel;

   //spi
   reg                          spi_m_sel, spi_s_sel;
   wire [31:0]                  spi_m_read_data, spi_s_read_data;

   wire                         ss;
   wire                         mosi;
   wire                         miso;
   
   
   // reset control counter 
   reg [15:0]                          rst_cnt;

   // reset control
   always @(posedge clk)
     if(~resetn)
       rst_cnt <= 16'd0;
     else if (rst_cnt < (16'd50))
       rst_cnt <= rst_cnt + 1'b1;

   assign resetn_int = (rst_cnt == (16'd50));


   picorv32 picorv32_core (
		           .clk         (clk         ),
		           .resetn      (resetn_int  ),
		           .trap        (trap        ),
		           .mem_valid   (mem_valid   ),
		           .mem_instr   (mem_instr   ),
		           .mem_ready   (mem_ready   ),
		           .mem_addr    (mem_addr    ),
		           .mem_wdata   (mem_wdata   ),
		           .mem_wstrb   (mem_wstrb   ),
		           .mem_rdata   (mem_rdata   ),
		           .mem_la_read (mem_la_read ),
		           .mem_la_write(mem_la_write),
		           .mem_la_addr (mem_la_addr ),
		           .mem_la_wdata(mem_la_wdata),
		           .mem_la_wstrb(mem_la_wstrb)
	                   );

   simpleuart simpleuart (
                          //serial i/f
		          .ser_tx      (ser_tx          ),
		          .ser_rx      (ser_rx          ),

                          //data bus
		          .clk         (clk             ),
		          .resetn      (resetn_int      ),

                          .address     (mem_addr[3:2]   ),
		          .sel         (uart_sel        ),	
	                  .we          (|mem_wstrb      ),
		          .dat_di      (mem_wdata       ),
		          .dat_do      (uart_read_data  )
	                  );

   spi_master spi_m (
                     //CONTROL INTERFACE
                     .clk        (clk),
                     .rst        (~resetn_int),
                     
                     .data_in    (mem_wdata),
                     .data_out   (spi_m_read_data),
                     .address    (mem_addr[4:2]),
                     .sel        (spi_m_sel),
                     .read       (spi_m_sel & ~(|mem_wstrb)),
                     .write      (|mem_wstrb),
                     .interrupt  (),

                     //SPI INTERFACE
                     .sclk       (sclk),
                     .ss         (ss),
                     .mosi       (mosi),
                     .miso       (miso)          
                     );
   
   spi_slave spi_s (
                    //CONTROL INTERFACE
                    .clk        (clk),
                    .rst        (~resetn_int),
                    .data_in    (mem_wdata),
                    .data_out   (spi_s_read_data),
                    .address    (mem_addr[4:2]),
                    .write      (|mem_wstrb),
                    .read       (spi_s_sel & ~(|mem_wstrb)),
                    .sel        (spi_s_sel),
                    .interrupt  (),

                    //SPI INTERFACE
                    .sclk       (sclk),
                    .ss         (ss),
                    .mosi       (mosi),
                    .miso       (miso)                    
                    );
   

   //
   // ADDRESS DECODER
   //
   
   // select peripherals
   always @* begin
      mem_sel = 1'b0;
      uart_sel = 1'b0;
      led_sel = 1'b0;
      spi_m_sel = 1'b0;
      spi_s_sel = 1'b0;
      

      if( (mem_addr>>2) < 2**MEM_ADDR_W) begin  //memory
         mem_sel = mem_valid;
      end else if(mem_addr == 32'h1000_0100) begin //led
         led_sel = mem_valid;                               
      end else if(mem_addr ==  32'h1000_0000 ||   //uart           
                  mem_addr ==  32'h1000_0004 || 
                  mem_addr ==  32'h1000_0008 ||
                  mem_addr ==  32'h1000_000C 
                  ) 
        begin 
           uart_sel = mem_valid;
      end else if(mem_addr ==  32'h1000_0104 ||   //spi_master
                  mem_addr ==  32'h1000_0108 || 
                  mem_addr ==  32'h1000_010C ||
                  mem_addr ==  32'h1000_0114 ||
                  mem_addr ==  32'h1000_0118 ||
                  mem_addr ==  32'h1000_011C
                  ) 
        begin 
           spi_m_sel = mem_valid;
      end else if(mem_addr ==  32'h1000_0204 ||   //spi_master
                  mem_addr ==  32'h1000_0208 || 
                  mem_addr ==  32'h1000_020C ||
                  mem_addr ==  32'h1000_0214 ||
                  mem_addr ==  32'h1000_0218 ||
                  mem_addr ==  32'h1000_021C
                  ) 
        begin 
           spi_s_sel = mem_valid;
        end
   end


   // send memory ready in the next cycle
   assign mem_ready_nxt = mem_sel | uart_sel | led_sel | spi_m_sel | spi_s_sel;
   always @(posedge clk, negedge resetn_int)
     if(~resetn_int) begin 
       mem_ready <= 1'b0;
     end else begin 
       mem_ready <= mem_ready_nxt;
     end
   
   // memory read data multiplexer
   always @*
     if(uart_sel)
       mem_rdata = uart_read_data;
     else if (spi_m_sel)
       mem_rdata = spi_m_read_data;
     else if (spi_s_sel)
       mem_rdata = spi_s_read_data;
     else
       mem_rdata =  mem_read_data;
 
   // LED
   always @(posedge clk)
      if(~resetn_int)
        led <= 1'b0; 
      else if (led_sel && |mem_wstrb)
        led <= mem_wdata[0];

   
   // MEMORY SYSTEM

   // byte 0
   xalt_1p_mem  #(
                   .MEM_INIT_FILE({"firmware", "_0", ".hex"}),
                   .DATA_W(8),
                   .ADDR_W(MEM_ADDR_W))
   byte_mem0
     (
      .data_a(mem_wdata[7:0]),
      .addr_a(mem_addr[MEM_ADDR_W+1:2]),
      .we_a(mem_sel & mem_wstrb[0]),
      .q_a(mem_read_data[7:0]),
      .clk(clk)
      );

   //byte 1
   xalt_1p_mem  #(
                   .MEM_INIT_FILE({"firmware", "_1", ".hex"}),
                   .DATA_W(8),
                   .ADDR_W(MEM_ADDR_W))
   byte_mem1
     (
      .data_a(mem_wdata[15:8]),
      .addr_a(mem_addr[MEM_ADDR_W+1:2]),
      .we_a(mem_sel & mem_wstrb[1]),
      .q_a(mem_read_data[15:8]),
      .clk(clk)
      );

   // byte 2
   xalt_1p_mem  #(
                   .MEM_INIT_FILE({"firmware", "_2", ".hex"}),
                   .DATA_W(8),
                   .ADDR_W(MEM_ADDR_W))
   byte_mem2
     (
      .data_a(mem_wdata[23:16]),
      .addr_a(mem_addr[MEM_ADDR_W+1:2]),
      .we_a(mem_sel & mem_wstrb[2]),
      .q_a(mem_read_data[23:16]),
      .clk(clk)
      );

   //byte 3
   xalt_1p_mem  #(
                   .MEM_INIT_FILE({"firmware", "_3", ".hex"}),
                   .DATA_W(8),
                   .ADDR_W(MEM_ADDR_W))
   byte_mem3
     (
      .data_a(mem_wdata[31:24]),
      .addr_a(mem_addr[MEM_ADDR_W+1:2]),
      .we_a(mem_sel & mem_wstrb[3]),
      .q_a(mem_read_data[31:24]),
      .clk(clk)
      );

endmodule

`timescale 1 ns / 1 ps

module system_tb;
   reg clk = 1;
   always #5 clk = ~clk;

   reg sclk = 1;
   always #23 sclk = ~sclk;

   reg resetn = 0;


   initial begin
      if ($test$plusargs("vcd")) begin
	 $dumpfile("system.vcd");
	 $dumpvars(0, system_tb);
      end
      repeat (100) @(posedge clk);
      resetn <= 1;
   end

   wire led;
   wire ser_tx;
   wire trap;

   system uut (
	       .clk        (clk        ),
	       .resetn     (resetn     ),
	       .led        (led        ),
	       .ser_tx     (ser_tx     ),
               .trap       (trap       ),
               .sclk       (sclk       )
	       );

   always @(posedge clk) begin
      if (resetn && trap) begin
	 $finish;
      end
   end
endmodule

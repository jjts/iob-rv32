`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/08/2019 03:14:20 PM
// Design Name: 
// Module Name: top_system
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_system(
	           input      C0_SYS_CLK_clk_p, C0_SYS_CLK_clk_n,	  
	           input      resetn,
	           output     led,
	           output     ser_tx,
                   output     trap
    );
    
    
       //single ended clock
   wire 		  clk;
   wire 		  clk_ibufg;
    
   IBUFGDS ibufg_inst (.I(C0_SYS_CLK_clk_p), .IB(C0_SYS_CLK_clk_n), .O(clk_ibufg));
   BUFG bufg_inst (.I(clk_ibufg), .O(clk));
   
      system system (
		           .clk         (clk    ),
		           .resetn      (resetn ),
		           .led         (led    ),
		           .ser_tx      (ser_tx ),
		           .trap        (trap   )
	                 );
   
endmodule

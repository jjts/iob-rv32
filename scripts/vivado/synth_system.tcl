
read_verilog top_system.v
read_verilog system.v
read_verilog ../../picorv32.v
read_verilog ../../picosoc/simpleuart.v
read_verilog ../../rtl/xalt_1p_mem.v

read_xdc synth_system.xdc

#            -part 'part fpga'         -top 'sistema de top'
synth_design -part xcku040-fbva676-1-c -top top_system 
opt_design
place_design
route_design

report_utilization
report_timing

write_verilog -force synth_system.v
write_bitstream -force synth_system.bit
# write_mem_info -force synth_system.mmi


//led
void led_set(unsigned char led)
{
  *((volatile int*) 0x10000100) = led;
}

//uart
#define uart_reset() (*((volatile int*) 0x1000000C) = 1)

void uart_setdiv(int div)
{
  *((volatile int*) 0x10000004) = div;
}

int uart_getwait()
{
  return *((volatile int*) 0x10000000);
}

void uart_wait()
{
  led_set(1);
  while(uart_getwait());
  led_set(0);
}

int uart_getdiv()
{
  return *((volatile int*) 0x10000004);
}

void uart_putc(char c)
{
  led_set(1);
  while( *( (volatile int *) 0x10000000) );
  led_set(0);

  *( (volatile int *) 0x10000008 ) = (int) c;
}

void uart_puts(const char *s)
{
  while (*s) uart_putc(*s++);
}

void uart_printf(const char* fmt, int var) {

  const char *w = fmt;
  char c;

  static unsigned long v;
  static unsigned long digit;
  static int digit_shift;
  static char hex_a = 'a';

  while ((c = *w++) != '\0') {
    if (c != '%') {
      /* Regular character */
      uart_putc(c);
    }
    else {
      c = *w++;
      switch (c) {
      case '%': // %%
      case 'c': // %c
        uart_putc(c);
        break;
      case 'X': // %X
        hex_a = 'A';  // Capital "%x"
      case 'x': // %x
          /* Process hexadecimal number format. */
          /* If the number value is zero, just print and continue. */
          if (var == 0)
            {
              uart_putc('0');
              continue;
            }

          /* Find first non-zero digit. */
          digit_shift = 28;
          while (!(var & (0xF << digit_shift))) {
            digit_shift -= 4;
          }

          /* Print digits. */
          for (; digit_shift >= 0; digit_shift -= 4)
            {
              digit = (var & (0xF << digit_shift)) >> digit_shift;
              if (digit <= 9) {
                c = '0' + digit;
              }
              else {
                c = hex_a + digit - 10;
              }
              uart_putc(c);
            }

          /* Reset the A character */
          hex_a = 'a';
          break;
        default:
          /* Unsupported format character! */
          break;
      }
    }
  }
}


//spi 
#define SPI_MASTER_TOP_0_BASE 0x10000100
#define SPI_SLAVE_TOP_0_BASE 0x10000200

#define SPI_READY 1
#define SPI_TXDATA 2
#define SPI_RXDATA 3
#define SPI_VERSION 5
#define SPI_SOFTRESET 6
#define SPI_DUMMY 7

#define RAM_SET32(base, location, value) *((volatile int*) (base + (sizeof(int)) * location)) = value
#define RAM_GET32(base, location)        *((volatile int*) (base + (sizeof(int)) * location))

void spiMasterInit(void)
{
  if (RAM_GET32(SPI_MASTER_TOP_0_BASE, SPI_READY) == 0)
    uart_puts("spi master not ready.\n");

  // check processor interface
  // write dummy register
  RAM_SET32(SPI_MASTER_TOP_0_BASE, SPI_DUMMY, 0xDEADBEEF);

  // read and check result 
  if (RAM_GET32(SPI_MASTER_TOP_0_BASE, SPI_DUMMY) != 0xDEADBEEF)
    uart_puts("spi master dummy reg test failed\n");
}

void spiMasterWrite(int w)
{
  // set tx register
  RAM_SET32(SPI_MASTER_TOP_0_BASE, SPI_TXDATA, w);
  // wait until all bits are transmitted
  while (RAM_GET32(SPI_MASTER_TOP_0_BASE, SPI_READY) == 0) ;
}

int spiMasterRead()
{
  // reset the tx register to init a read spi cycle
  RAM_SET32(SPI_MASTER_TOP_0_BASE, SPI_TXDATA, 0);
  // wait until cycle complete
  while (RAM_GET32(SPI_MASTER_TOP_0_BASE, SPI_READY) == 0) ;
}

void spiSlaveWrite(int w)
{
  // set tx register
  RAM_SET32(SPI_SLAVE_TOP_0_BASE, SPI_TXDATA, w);
}


int spiSlaveRead(void)
{
  int srw;

  // wait until ready to read 
  while (RAM_GET32(SPI_SLAVE_TOP_0_BASE, SPI_READY) == 0) ;

  // read received word 
  srw = RAM_GET32(SPI_SLAVE_TOP_0_BASE, SPI_RXDATA);

  return srw;
}


//lib
void *memcpy(void *dest, const void *src, int n)
{
  while (n) {
    n--;
    ((char*)dest)[n] = ((char*)src)[n];
  }
  return dest;
}

//main program
void main()
{ 
  // setup uart
  uart_reset();
  //*((volatile int*) 0x1000000C) = 1;

  //for 100MHz
  //uart_setdiv(868);

  //for 50MHz
  //uart_setdiv(434);
  //if(uart_getdiv() != 868) led_set(1);
  
  //for 250MHz
  uart_setdiv(2170); //250M:115200(baud-rate)

  uart_wait();

  // send message
  uart_puts("Hello World!\n");
  uart_wait();

  while(1);

  // init spi 
  uart_puts("init spi\n");
  spiMasterInit();
  uart_puts("done\n");

  int txw = 0x1234567;
  uart_puts("spi M send\n");
  spiMasterWrite(txw);
  uart_puts("done\n");

  int rxw;
  uart_puts("spi S rcvd\n");
  rxw = spiSlaveRead();
  uart_puts("done\n");

  if(rxw != txw)
    uart_printf("FAILED: %x\n", rxw);

  uart_printf("rxw = %x\n", rxw);
}
